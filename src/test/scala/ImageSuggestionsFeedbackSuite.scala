import com.datastax.driver.core.utils.UUIDs
import com.google.common.io.Resources
import io.findify.flink.api._
import io.findify.flinkadt.api._
import org.apache.flink.api.java.utils.ParameterTool
import org.apache.flink.runtime.testutils.MiniClusterResourceConfiguration
import org.apache.flink.test.util.MiniClusterWithClientResource
import org.apache.flink.types.Row
import org.scalatest.BeforeAndAfter
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import org.wikimedia.dataplatform._
import org.wikimedia.eventutilities.flink.stream.EventDataStreamFactory

import java.lang
import java.nio.charset.StandardCharsets
import scala.collection.JavaConverters._

class ImageSuggestionsFeedbackSuite extends AnyFlatSpec with Matchers with BeforeAndAfter {

  val flinkCluster = new MiniClusterWithClientResource(new MiniClusterResourceConfiguration.Builder()
    .setNumberSlotsPerTaskManager(2)
    .setNumberTaskManagers(1)
    .build)
  val JobName = "ImageSuggestionsFeedbackMiniCluster"
  implicit val parameters: ParameterTool = ParameterTool.fromPropertiesFile(
    "src/test/resources/test.config.properties"
  )

  private val schemaBaseUris = List(Resources.getResource("event-schemas/repo").toString)
  private val testStreamConfigsFile = Resources.getResource("feedback_stream_config.json").toString
  private val factory = EventDataStreamFactory.from(schemaBaseUris.asJava, testStreamConfigsFile)

  before {
    flinkCluster.before()
  }

  after {
    flinkCluster.after()
  }

  "ImageSuggestionFeedback pipeline" should "process message correctly" in {

    val env = StreamExecutionEnvironment.getExecutionEnvironment
    env.setParallelism(2)
    env.getConfig.setGlobalJobParameters(parameters)

    val input =
      """
        |    {
        |      "$schema": "/mediawiki/page/image-suggestions-feedback/1.0.0",
        |      "wiki": "enwiki",
        |      "page_id": 1234,
        |      "filename": "https://commons.wikimedia.org/wiki/File:example.JPEG",
        |      "origin_wiki": "commons",
        |      "user_id": 1234,
        |      "is_accepted": false,
        |      "is_rejected": true,
        |      "rejection_reason": "Incorrect Image",
        |      "dt": "2022-04-13T14:12:16.372Z",
        |      "meta": {
        |        "dt": "2022-04-13T14:12:16.372Z",
        |        "stream": "/mediawiki/page/image-suggestions-feedback"
        |      }
        |    }
        |""".stripMargin

    val expected = Row.of(
      "enwiki",
      new Integer(1234),
      "example.JPEG",
      UUIDs.random(),
      "commons",
      new Integer(1234),
      new lang.Boolean(false),
      new lang.Boolean(true),
      "Incorrect Image"
    )

    val deserializer = factory.deserializer("mediawiki.image-suggestions-feedback")

    val out = ImageSuggestionsFeedback
      .transform(
        env.fromElements(input)
          .map((jsonString: String) => deserializer.deserialize(jsonString.getBytes(StandardCharsets.UTF_8)))(
            factory.rowTypeInfo("mediawiki.image-suggestions-feedback"))
      )
      .executeAndCollect()
      .next()

    compareIgnoreField(expected, out, 3) shouldBe true
  }

  it should "filter out canary events" in {

    val env = StreamExecutionEnvironment.getExecutionEnvironment
    env.setParallelism(2)
    env.getConfig.setGlobalJobParameters(parameters)

    val input =
      """
        |    {
        |      "$schema": "/mediawiki/page/image-suggestions-feedback/1.0.0",
        |      "wiki": "enwiki",
        |      "page_id": 1234,
        |      "filename": "https://commons.wikimedia.org/wiki/File:example.JPEG",
        |      "origin_wiki": "commons",
        |      "user_id": 1234,
        |      "is_accepted": false,
        |      "is_rejected": true,
        |      "rejection_reason": "Incorrect Image",
        |      "dt": "2022-04-13T14:12:16.372Z",
        |      "meta": {
        |        "dt": "2022-04-13T14:12:16.372Z",
        |        "stream": "/mediawiki/page/image-suggestions-feedback",
        |        "domain": "canary"
        |      }
        |    }
        |""".stripMargin

    val deserializer = factory.deserializer("mediawiki.image-suggestions-feedback")

    val out = ImageSuggestionsFeedback
      .transform(
        env.fromElements(input)
          .map((jsonString: String) => deserializer.deserialize(jsonString.getBytes(StandardCharsets.UTF_8)))(
            factory.rowTypeInfo("mediawiki.image-suggestions-feedback"))
      )
      .executeAndCollect()
      .hasNext

    out shouldBe false
  }

  def compareIgnoreField(o1: Row, o2: Row, fieldIndex: Int): Boolean = {
    o1.setField(fieldIndex, o2.getField(fieldIndex))
    o1.toString == o2.toString
  }

}
