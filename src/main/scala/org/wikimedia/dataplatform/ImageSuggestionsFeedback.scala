package org.wikimedia.dataplatform

import com.datastax.driver.core.Cluster
import com.datastax.driver.core.utils.UUIDs
import io.findify.flink.api._
import io.findify.flinkadt.api.{boolInfo, intInfo, stringInfo}
import org.apache.flink.api.common.eventtime.WatermarkStrategy
import org.apache.flink.api.common.typeinfo.TypeInformation
import org.apache.flink.api.java.typeutils.RowTypeInfo
import org.apache.flink.api.java.utils.ParameterTool
import org.apache.flink.connector.kafka.source.KafkaSource
import org.apache.flink.streaming.connectors.cassandra.CassandraSink.CassandraRowSinkBuilder
import org.apache.flink.streaming.connectors.cassandra.ClusterBuilder
import org.apache.flink.types.Row
import org.slf4j.{Logger, LoggerFactory}
import org.wikimedia.eventutilities.flink.stream.EventDataStreamFactory

import java.util.UUID
import scala.collection.JavaConverters._

object cassandraRowTypeInfo {
  val typeInfo: RowTypeInfo = new RowTypeInfo(
    Array[TypeInformation[_]](
      stringInfo, intInfo, stringInfo, TypeInformation.of(classOf[UUID]),
      stringInfo, intInfo, boolInfo, boolInfo, stringInfo
    ),
    Array[String](
      "wiki", "page_id", "filename", "id",
      "origin_wiki", "user_id", "is_accepted", "is_rejected", "rejection_reason"
    )
  )
}

class ImageSuggestionsFeedback

object ImageSuggestionsFeedback {

  val Log: Logger = LoggerFactory.getLogger(classOf[ImageSuggestionsFeedback])

  private def sinkToCassandra(dataStream: DataStream[Row])(implicit parameters: ParameterTool): Unit = {
    val cassandraHost = parameters.getRequired("CASSANDRA_HOST")
    val cassandraPort = parameters.getRequired("CASSANDRA_PORT").toInt
    val cassandraUser = parameters.getRequired("CASSANDRA_USERNAME")
    val cassandraPass = parameters.getRequired("CASSANDRA_PASSWORD")

    val cassandraTable = parameters.getRequired("CASSANDRA_TABLE")
    val fieldNames = cassandraRowTypeInfo.typeInfo.getFieldNames
    val cassandraQuery =  s"""INSERT INTO $cassandraTable(${fieldNames.mkString(",")})
                             | values(${fieldNames.map(_ => "?").mkString(",")})
                             | """.stripMargin

    new CassandraRowSinkBuilder(
      dataStream.javaStream,
      cassandraRowTypeInfo.typeInfo,
      cassandraRowTypeInfo.typeInfo.createSerializer(
        dataStream.javaStream.getExecutionEnvironment.getConfig
      )
    )
      .setQuery(cassandraQuery)
      .setClusterBuilder(
        new ClusterBuilder() {
          def buildCluster(builder: Cluster.Builder): Cluster = {
            builder
              .addContactPoint(cassandraHost)
              .withPort(cassandraPort)
              .withCredentials(cassandraUser, cassandraPass)
              .build
          }
        }
      )
      .build()
  }

  private def buildKafkaWikiSource(implicit parameters: ParameterTool): (KafkaSource[Row], EventDataStreamFactory) = {
    val kafkaBootstrapServers = parameters.getRequired("KAFKA_BOOTSTRAP_SERVERS")
    val kafkaGroupId = parameters.getRequired("KAFKA_GROUP_ID")
    val kafkaStreamName = parameters.getRequired("KAFKA_STREAM_NAME")

    val kafkaSourceRoutes = Option(parameters.get("KAFKA_SOURCE_ROUTES"))
    val kafkaClientRoutes = Option(parameters.get("KAFKA_CLIENT_ROUTES"))

    val eventSchemaBaseUris = parameters.getRequired("EVENT_SCHEMA_BASE_URIS").split(",")
    val eventStreamConfigUri = parameters.getRequired("EVENT_STREAM_CONFIG_URI")
    val kafkaHttpClientRoutes = (kafkaSourceRoutes, kafkaClientRoutes) match {
      case (Some(source), Some(client)) =>
        Option(
          source.split(",")
            .zip(client.split(","))
            .toMap.asJava
        )
      case _ => None
    }

    val factory = EventDataStreamFactory.from(
      eventSchemaBaseUris.toList.asJava,
      eventStreamConfigUri,
      kafkaHttpClientRoutes.orNull
    )
    val kafkaSource = factory.kafkaSourceBuilder(
      kafkaStreamName,
      kafkaBootstrapServers,
      kafkaGroupId
    )
      .build()

    (kafkaSource, factory)
  }

  private def getProperties(args: Array[String]): ParameterTool = {
    val envParams = ParameterTool.fromMap(System.getenv())
    val argsParams = ParameterTool.fromArgs(args)
    val mergedParams = argsParams.mergeWith(envParams)
    val propertiesFile = Option(mergedParams.get("PROPERTIES_FILE"))

    propertiesFile match {
      case None => mergedParams
      case Some(value) => ParameterTool.fromPropertiesFile(value)
        .mergeWith(argsParams)
        .mergeWith(envParams)
    }
  }

  def transform(dataStream: DataStream[Row]): DataStream[Row] = {
    val rowTypeInfo: RowTypeInfo = dataStream.dataType.asInstanceOf[RowTypeInfo]
    val sinkRowTypeInfo = cassandraRowTypeInfo.typeInfo

    dataStream
      .filter(row => Option(row.getFieldAs[Row]("meta").getFieldAs[String]("domain")) match {
        case Some(value) => value != "canary"
        case _ => true
      })
      .map(row => {
        // The row received is a hybrid row, which doesn't allow addition of new fields
        // so we map the fields we need into a field-based row
        // NOTE: Row.project does not work on hybrid rows if there's an unknown column name
        // which in this case is the id
        val fieldRow = Row.withNames()
        val fieldNames = sinkRowTypeInfo.getFieldNames
        for (name <- fieldNames) {
          if (rowTypeInfo.hasField(name)) {
            fieldRow.setField(name, row.getField(name))
          }
        }

        // Add row field needed that wasn't populated above
        fieldRow.setField("id", UUIDs.timeBased())

        // (Currently) the value for `filename` is a full url, so we fetch the filename
        fieldRow.setField("filename", row.getFieldAs[String]("filename").split("File:").last)

        // Numbers are by default type Long, which cannot be inserted into Cassandra int columns
        fieldRow.setField("page_id", Math.toIntExact(row.getFieldAs[Long]("page_id")))
        fieldRow.setField("user_id", Math.toIntExact(row.getFieldAs[Long]("user_id")))

        // Filter and reorder required columns
        val filteredFieldRow = Row.project(fieldRow, fieldNames)

        // Convert filteredFieldRow into a position-based row which is insertable into Cassandra
        // TODO: Use named-position row so we don't have to do this
        val positionRow = Row.withPositions(filteredFieldRow.getArity)
        fieldNames.zipWithIndex.foreach {
          case (field, idx) => positionRow.setField(idx, filteredFieldRow.getField(field))
        }
        positionRow
      }
    )(sinkRowTypeInfo)
  }

  def main(args: Array[String]): Unit = {
    val env = StreamExecutionEnvironment.getExecutionEnvironment
    implicit val parameters: ParameterTool = getProperties(args)
    env.getConfig.setGlobalJobParameters(
      parameters
    )

    val kafkaStreamName = parameters.getRequired("KAFKA_STREAM_NAME")
    val (kafkaSource, eventDataStreamFactory) = buildKafkaWikiSource
    val stream = env.fromSource(
      kafkaSource,
      WatermarkStrategy.noWatermarks(),
      parameters.getRequired("FLINK_SOURCE_NAME")
    )(eventDataStreamFactory.rowTypeInfo(kafkaStreamName))


    sinkToCassandra(
      transform(stream)
    )
    env.execute()
  }
}
